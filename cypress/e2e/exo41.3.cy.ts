describe('Exo42 test', () => {
  it('Test 3', () => {
    cy.visit('http://localhost:3000/build/Exercice-41');
    cy.get("input#newItem").type("<script>alert(\"hack\")</script>");
    cy.get("input[name = push]").click();
    cy.get("#lifo").children().should("have.length", 1);
    cy.get("#lifo").should("contain", "<script>alert(\"hack\")</script>");
  })
})
