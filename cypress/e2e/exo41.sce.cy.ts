describe('Exo42 test', () => {
  it('Scénario demandé par le prof', () => {
    // Vous vous connectez sur la page
    cy.visit('http://localhost:3000/build/Exercice-41');
    // Vous Ajoutez un premier élément dans la fifo et vérifier qu’il y en a bien un de plus
    cy.get("input#newItem").type("obj1");
    cy.get("input[name = push]").click();
    cy.get("#lifo").children().should("have.length", 1);
    // Vous Ajoutez un deuxième élément dans la fifo et vérifier qu’il y en a bien un de plus
    cy.get("input#newItem").type("obj2");
    cy.get("input[name = push]").click();
    cy.get("#lifo").children().should("have.length", 2);
    // Après un POP il doit y en avoir un de moins
    cy.get("input[name = pop]").click();
    cy.get("#lifo").children().should("have.length", 1);

    // Après un PEEK il doit y en avoir toujours autant et le premier élément est dans la zone prévue à cet effet
    cy.get("input[name = peek]").click();
    cy.get("#lifo").children().should("have.length", 1);
    cy.should('exist', cy.get('#peek-area').contains('obj1'));

    // Après un POP la fifo doit être vide.
    cy.get("input[name = pop]").click();
    cy.get("#lifo").children().should("have.length", 0);
  })
})
