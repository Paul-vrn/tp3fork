describe('Exo42 test', () => {
  it('Test 0', () => {
    cy.visit('http://localhost:3000/build/Exercice-41');
    cy.get("input#newItem").type("test");
    cy.get("input[name = push]").click();
    cy.get("input[name = peek]").click();
    cy.should('exist', cy.get("#lifo").contains("test"));
  })
})
