describe('Exo42 test', () => {
  it('Test 2', () => {
    cy.visit('http://localhost:3000/build/Exercice-41');
    cy.get("input#newItem").type("test");
    cy.get("input[name = push]").click();
    cy.get("input[name = pop]").click();
    cy.get("#lifo").children().should("have.length", 0);
    cy.get("#lifo").should("not.contain", "test");
  })
})
