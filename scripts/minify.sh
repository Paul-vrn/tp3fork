#!/bin/bash
if [[ ! -d build ]]; then
  mkdir build
fi;
for f in *.{html,css};
do
  npx minify "$f" > "build/$f"
done;
for f in js/*.js;
do
  npx uglifyjs "$f" -c -m --output "build/"$(basename "$f")
done;
  npx uglifyjs "Exercice-42-defs.js" -c -m --output "build/"$(basename "Exercice-42-defs.js")

