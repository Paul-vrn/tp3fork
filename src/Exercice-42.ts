/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
declare const getDefinition: any;

/**
 * Interface for a list definition
 * @property title the title of the list
 * @property items an array of items which can be a string or a list definition
 */
interface ListeDefinition {
  title: string;
  items: (ListeDefinition | string)[];
}

window.addEventListener("load", () => {
  /* global getDefinition */
  const contents: HTMLDivElement = document.getElementById("contents") as HTMLDivElement;
  const listi = (i: number) => {
    if (contents.firstChild) {
      contents.removeChild(contents.firstChild);
    }
    contents.appendChild(constructList(getDefinition(i)));
  };
  const radioButtons = document.querySelectorAll(
    "div#commands input[type=radio]"
  ) as NodeListOf<HTMLInputElement>;
  radioButtons.forEach((radioButton: HTMLInputElement) => {
    radioButton.addEventListener("change", () => {
      if (radioButton.checked) {
        listi(parseInt(radioButton.value));
      }
    });
  });

  /**
   *  Construct recursively a list from a definition
   * @param definition the entry point as a definition
   * @returns the list
   */
  const constructList = (definition: ListeDefinition): HTMLDListElement => {
    const dl: HTMLDListElement = document.createElement("dl");
    const dt: HTMLElement = document.createElement("dt");
    dt.textContent = definition.title;
    dl.appendChild(dt); // add the title
    if (definition.items.length > 0) {
      const dd = document.createElement("dd");
      dl.appendChild(dd); // add the list
      const ul: HTMLUListElement = document.createElement("ul");
      dd.appendChild(ul);
      definition.items.forEach((item: ListeDefinition | string) => {
        const li: HTMLLIElement = document.createElement("li");
        if (typeof item === "string") {
          li.textContent = item;
        } else {
          li.appendChild(constructList(item)); // recursive call
        }
        ul.appendChild(li);
      });
    }
    return dl;
  };
});
