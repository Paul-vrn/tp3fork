window.addEventListener("load", () => {
  const ulLifo: HTMLUListElement = document.getElementById("lifo") as HTMLUListElement;
  const textInput: HTMLInputElement = document.getElementById("newItem") as HTMLInputElement;
  const divPeakArea: HTMLDivElement = document.getElementById("peek-area") as HTMLDivElement;
  const lifo_push = () => {
    const newItem: string = textInput.value;
    if (newItem === "") {
      alert("Zone de saisie vide.");
      throw Error("Veuillez saisir un élément.");
    }
    const newLi: HTMLLIElement = document.createElement("li");
    newLi.textContent = newItem; // on utilise textContent à la place de innerHTML pour éviter les injections de code
    // add first
    ulLifo.prepend(newLi); // ajoute l'élément en premier
    textInput.value = ""; // reste la valeur de l'input text
  };
  const lifo_pop = () => {
    if (!ulLifo.hasChildNodes()) {
      alert("Aucun élément à supprimer.");
      throw Error("Aucun élément à supprimer.");
    }
    ulLifo.removeChild(ulLifo.firstChild!);
  };
  const lifo_peek = () => {
    divPeakArea.textContent = "";
    if (!ulLifo.hasChildNodes()) {
      alert("Aucun élément à afficher.");
      throw Error("Aucun élément à supprimer.");
    }
    divPeakArea.textContent = ulLifo.firstChild!.textContent!;
  };
  // On met le long chemin "div#commands form" pour éviter d'intérférer sur d'autres potentiels formulaires
  document.querySelector("div#commands form")!.addEventListener("submit", event => {
    event.preventDefault();
  });
  document.querySelector("div#commands input[name = push]")!.addEventListener("click", lifo_push);
  document.querySelector("div#commands input[name = pop]")!.addEventListener("click", lifo_pop);
  document.querySelector("div#commands input[name = peek]")!.addEventListener("click", lifo_peek);
});
