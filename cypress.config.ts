import { defineConfig } from "cypress";

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:3000',
    projectId: '7ji9e7', // this is the project id from the URL that connect to cypress dashboard
    video: false,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
