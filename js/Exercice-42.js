"use strict";
window.addEventListener("load", () => {
  /* global getDefinition */
  const contents = document.getElementById("contents");
  const listi = i => {
    if (contents.firstChild) {
      contents.removeChild(contents.firstChild);
    }
    contents.appendChild(constructList(getDefinition(i)));
  };
  const radioButtons = document.querySelectorAll("div#commands input[type=radio]");
  radioButtons.forEach(radioButton => {
    radioButton.addEventListener("change", () => {
      if (radioButton.checked) {
        listi(parseInt(radioButton.value));
      }
    });
  });
  /**
   *  Construct recursively a list from a definition
   * @param definition the entry point as a definition
   * @returns the list
   */
  const constructList = definition => {
    const dl = document.createElement("dl");
    const dt = document.createElement("dt");
    dt.textContent = definition.title;
    dl.appendChild(dt); // add the title
    if (definition.items.length > 0) {
      const dd = document.createElement("dd");
      dl.appendChild(dd); // add the list
      const ul = document.createElement("ul");
      dd.appendChild(ul);
      definition.items.forEach(item => {
        const li = document.createElement("li");
        if (typeof item === "string") {
          li.textContent = item;
        } else {
          li.appendChild(constructList(item)); // recursive call
        }
        ul.appendChild(li);
      });
    }
    return dl;
  };
});
