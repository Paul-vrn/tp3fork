# TP3JSDOM

## Cliquez sur les badges pour voir les rapports

[![TP3JSON](https://img.shields.io/endpoint?url=https://cloud.cypress.io/badge/simple/7ji9e7&style=flat&logo=cypress)](https://cloud.cypress.io/projects/7ji9e7/runs)
[![eslint](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP3_JSDOM/TP3JSDOM_paul_vernin/badge_eslint.svg)](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP3_JSDOM/TP3JSDOM_paul_vernin/report_lint_js.html)

## Lien des sites

[Exercice-41](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP3_JSDOM/TP3JSDOM_paul_vernin/Exercice-41.html)
[Exercice-42](https://grenoble-inp-ensimag.gitlab.io/4MMCAW6/G4/TP3_JSDOM/TP3JSDOM_paul_vernin/Exercice-42.html)
